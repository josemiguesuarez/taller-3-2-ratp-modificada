module.exports = {
    staticFileGlobs: [
        'index.html',
        'manifest.json',
        'images/**.*',
        'scripts/**.*',
        'styles/**.*'
    ],
    stripPrefix: '/',
    runtimeCaching: [{
        urlPattern: /.*/,
        handler: 'networkFirst'
    }]
};